import React from 'react';

class Post extends React.Component {
    constructor(props) {
        super(props);
        this.state =
        {
            title: null,
            body: null,
        };
    }

    render() {
        return (
            <div>
                <h3>{this.props.title}</h3>
                <p>
                    <div dangerouslySetInnerHTML={{ __html: this.props.body }} />
                </p>
            </div>
        );
    }
}

export default Post;