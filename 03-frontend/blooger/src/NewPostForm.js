import React from 'react';

class NewPostForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { title: '', body: '' };
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleBodyChange = this.handleBodyChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleTitleChange(event) { this.setState({ title: event.target.value }); }
    handleBodyChange(event) { this.setState({ body: event.target.value }); }
    handleSubmit(event) {
        var postArr = JSON.parse(localStorage.getItem("posts"));
        if (postArr === null) {
            postArr = [];
        }
        postArr.push({ title: this.state.title, body: this.state.body });
        localStorage.setItem("posts", JSON.stringify(postArr));
        alert('New post ' + this.state.title + ' is now live!');
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <h3>New Post</h3>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        <b>Title:</b>
                        <br />
                        <input type="text" value={this.state.title} onChange={this.handleTitleChange} />
                    </label>
                    <br />
                    <label>
                        <b>Body:</b>
                        <br />
                        <textarea value={this.state.body} onChange={this.handleBodyChange} />
                    </label>
                    <br />
                    <input type="submit" value="Submit" />
                </form>

            </div>

        );
    }
}

export default NewPostForm;