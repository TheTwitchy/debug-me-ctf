import React from 'react';
import './App.css';
import NewPostForm from './NewPostForm';
import Post from './Post';
import sanitize from './Utils';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { postsData: null };
    var postsArr = JSON.parse(localStorage.getItem("posts"));
    if (postsArr !== null) {
      this.state.postsData = postsArr;
    }
  }

  render() {
    return <div className="App">
      <h1>Blooger</h1>
      <h4>The blogger app that makes you say 'Oooooo'</h4>
      <hr />
      <h1>Posts</h1>
      {this.state.postsData &&
        this.state.postsData.map((post) => <Post title={post.title} body={sanitize(post.body)}></Post>)
      }
      {!this.state.postsData &&
        <p>There are no posts. You should make one below!</p>
      }
      <hr />
      <NewPostForm></NewPostForm>
    </div>
  }
}

export default App;
