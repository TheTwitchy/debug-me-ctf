function sanitize(str){
    if (str.toLowerCase().includes("onerror")){
        const replaceRegex = /<(.)*>/;
        return str.replace(replaceRegex, "");
    }
    return str;
}

export default sanitize;