#include <sodium.h>
#include <string.h>

#define DECRYPTION_KEY "bestSecurityEver                "
#define SECURE_NONCE "bestNonceEver           "
#define DECRYPT_BUF_SIZE 128

#define LEVEL_1_PASSWORD_ENCRYPTED "DE1D1CA0D49F60BC32BC94C31B381BCDBE1F8B56DCA5A8C3E31221"
#define LEVEL_1_PASSWORD "[REDACTED]"
#define LEVEL_1_PASSWORD_LEN 11
#define LEVEL_1_CIPHERTEXT_LEN (crypto_secretbox_MACBYTES + LEVEL_1_PASSWORD_LEN)

int main(int argc, char ** argv)
{
    const char hexstring[] = LEVEL_1_PASSWORD_ENCRYPTED, *pos = hexstring;

    unsigned char ciphertext[LEVEL_1_CIPHERTEXT_LEN];
    unsigned char decryptBuffer[DECRYPT_BUF_SIZE];
    unsigned char passwordBuffer[32];
    size_t n;

    if (sodium_init() < 0)
    {
        printf("ERROR: libsodium could not be initialized. This is bad. Make sure it's installed.\n\n");
        return -1;
    }

    if (argc < 2){
        printf("ERROR: You did not give a password on the command line!\n\n");
        return -1;
    }

    if (strlen(argv[1]) > 32){
        printf("ERROR: Password is too long.\n\n");
        return -1;
    }

    /*
    crypto_secretbox_easy(ciphertext, (const unsigned char *) LEVEL_1_PASSWORD, LEVEL_1_PASSWORD_LEN, SECURE_NONCE, DECRYPTION_KEY);

    printf("DEBUG: ciphertext = 0x");
    n = 0;
    for (; n < LEVEL_1_CIPHERTEXT_LEN; n++){
        printf("%02X", ciphertext[n]);
    }
    printf("\n");
    */

    /* Level 1 */
    /* First convert the encrypted string into bytes... */
    for (n = 0; n < sizeof ciphertext/sizeof *ciphertext; n++) {
        sscanf(pos, "%2hhx", &ciphertext[n]);
        pos += 2;
    }
    /* ...then decrypt it... */
    if (crypto_secretbox_open_easy(decryptBuffer, ciphertext, LEVEL_1_CIPHERTEXT_LEN, SECURE_NONCE, DECRYPTION_KEY) != 0) {
        printf("ERROR: The encrypted password failed to decrypt, this is a bug.\n\n");
        return -1;
    }

    /* ...and check it against what we got from the user. */
    if (memcmp(decryptBuffer, argv[1], LEVEL_1_PASSWORD_LEN) == 0) {
        printf("INFO: Success! You got the password!\n\n");
        return 0;
    }

    /* Incorrect password given. */
    printf("ERROR: Incorrect password!\n\n");

    return 0;
}