package com.thetwitchy.debugmectf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {
    @Autowired
    private ApplicationContextProvider applicationContextProvider;

    
    @PostMapping(value = "/ping", consumes = {"application/json"})
    public String ping(@RequestBody PingCommand pingCmd) {
        CommandRunService cmdRunService = applicationContextProvider.getApplicationContext()
                    .getBean(CommandRunService.class);
        
        return cmdRunService.runCommand(pingCmd);
    }
}
