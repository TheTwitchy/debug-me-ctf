package com.thetwitchy.debugmectf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class CommandRunner2Impl implements CommandRunner{

    @Override
    public String run(AdminCommand cmd) {
        try {
            String[] args = cmd.getCmd().split(" ", 3);
            ProcessBuilder pb = new ProcessBuilder(args);
            pb.redirectErrorStream(true);

            Process p = pb.start();
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            
            String line = null;
            String finalOut = "";
            while ((line = reader.readLine()) != null) {
                finalOut += line;
            }

            return finalOut;
        } catch (IOException e) {
            e.printStackTrace();
            return "There was an error. Please check your input.";
        }
    }
}
