package com.thetwitchy.debugmectf;

import org.springframework.context.annotation.Scope;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CommandRunService {
    private CommandRunner runner;

    public CommandRunService(){
        this.runner = new CommandRunner2Impl();
    }

    public String runCommand(AdminCommand cmd){
        if (cmd.useLegacy()){
            this.runner = new CommandRunnerImpl();
        }
        
        return this.runner.run(cmd);
    }

}
