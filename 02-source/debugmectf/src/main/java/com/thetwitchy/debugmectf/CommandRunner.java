package com.thetwitchy.debugmectf;

public interface CommandRunner {
    public String run(AdminCommand cmd);
}
