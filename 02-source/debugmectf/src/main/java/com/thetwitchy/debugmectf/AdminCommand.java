package com.thetwitchy.debugmectf;

public class AdminCommand {
    private String user;
    private String cmd;
    private long time;
    private boolean useLegacy = false;

    public AdminCommand(String user, String cmd, long time) {
        this.user = user;
        this.cmd = cmd;
        this.time = time;
    }

    public AdminCommand() {
        this.user = "";
        this.cmd = "";
        this.time = 0L;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean useLegacy() {
        return this.useLegacy;
    }

    public void setUseLegacy(boolean useLegacy) {
        this.useLegacy = useLegacy;
    }

    public boolean validate() {
        // Check calling user
        if (!this.user.equals("rastley")) {
            return false;
        }

        // Check to ensure time run is correct (accounting for error)
        if (Math.abs(this.time - System.currentTimeMillis()) > 3600000L) {
            return false;
        }

        // All checks passed.
        return true;
    }

}