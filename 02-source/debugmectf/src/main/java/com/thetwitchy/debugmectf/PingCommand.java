package com.thetwitchy.debugmectf;

public class PingCommand extends AdminCommand {
    private String ipAddr;

    public PingCommand(){
    }

    public PingCommand(String ipAddr){
        this.ipAddr = ipAddr;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    @Override
    public String getCmd() {
        return "ping -c1 " + this.ipAddr;
    }
}
